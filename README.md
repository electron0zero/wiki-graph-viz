# README #

This project crawls Wikipedia, extracts link information from the paragraphs before the TOC, and then creates a graph out of it.
The graph (a small part of it) is then displayed using a d3 application

### Packages used ###

* BeautifulSoup4
* Neo4j
* py2neo
* Flask
* D3