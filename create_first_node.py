"""
This creates the 1st node in the wiki graph
"""

from py2neo import authenticate, Graph
if __name__ == "__main__":
    # authenticate and fetch graph object
    authenticate("localhost:7474", "neo4j", "dw7xJjUb$Jn$")
    graph = Graph()
    g = graph.cypher.execute("CREATE (n:Topic {is_processed:'False', title:'Philosophy', "
                             "url:'http://en.wikipedia.org/wiki/Philosophy',"
                             "incoming:0}) "
                             "RETURN n, n.title as title, n.url as url LIMIT 1")

    if g.one is None:
        print "oops"
    else:
        print "done"